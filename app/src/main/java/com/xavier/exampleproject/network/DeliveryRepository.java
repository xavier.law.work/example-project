package com.xavier.exampleproject.network;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.xavier.exampleproject.db.AppDB;
import com.xavier.exampleproject.model.Delivery;
import com.xavier.exampleproject.model.response.DeliveryResponse;
import com.xavier.exampleproject.utils.CallUtils;
import com.xavier.exampleproject.utils.Utils;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeliveryRepository {
    private static final String TAG = "DeliveryRepository";

    private static DeliveryRepository instance;

    public static DeliveryRepository getInstance(Application application) {
        if (instance == null) {
            instance = new DeliveryRepository(application);
        }
        return instance;
    }

    //api
    private DeliveryApi deliveryApi;
    private MutableLiveData<DeliveryResponse> apiDeliveryData;

    //db
    private AppDB appDB;
    private Executor executor;

    private DeliveryRepository(Application application) {
        deliveryApi = RetrofitService.createService(DeliveryApi.class);
        executor = Executors.newSingleThreadExecutor();
        appDB = AppDB.getInstance(application.getApplicationContext());
        apiDeliveryData = new MutableLiveData<>();
    }

    public MutableLiveData<DeliveryResponse> getDeliveryList(Context context, int offset, int limit) {
        if (Utils.isNetworkAvailable(context)) {
            // if has network
            CallUtils.enqueueWithRetry(deliveryApi.getDeliveryList(offset, limit), new Callback<DeliveryResponse>() {
                @Override
                public void onResponse(Call<DeliveryResponse> call, Response<DeliveryResponse> response) {
                    Log.d(TAG, "onResponse " + response.toString() + " " + response.isSuccessful());
                    if (response.isSuccessful()) {
                        apiDeliveryData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<DeliveryResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure " + t.toString());
                    apiDeliveryData.setValue(null);
                }
            });
        } else {
            Log.d(TAG, "isNetworkNotAvailable");
            // if no network
            apiDeliveryData.setValue(null);
        }
        return apiDeliveryData;
    }

    public LiveData<Delivery> getDeliveryLiveData(int deliveryId) {
        return appDB.deliveryDao().getDelivery(deliveryId);
    }

    public LiveData<List<Delivery>> getDeliveryListLiveData() {
        return appDB.deliveryDao().getAllDeliveryData();
    }

    public void insertInDB(Delivery delivery) {
        executor.execute(() -> appDB.deliveryDao().insert(delivery));
    }

    public void updateInDB(Delivery delivery) {
        executor.execute(() -> appDB.deliveryDao().update(delivery));
    }

    public static void destroyInstance() {
        instance = null;
    }
}
