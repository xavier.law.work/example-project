package com.xavier.exampleproject.network;

import com.xavier.exampleproject.model.response.DeliveryResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DeliveryApi {
    @GET("/deliveries")
    Call<DeliveryResponse> getDeliveryList(@Query("offset") int offset,
                                           @Query("limit") int limit);
}
