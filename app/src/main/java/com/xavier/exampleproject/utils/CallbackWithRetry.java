package com.xavier.exampleproject.utils;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class CallbackWithRetry<T> implements Callback<T> {
    private static final String TAG = "CallbackWithRetry";

    private static final int TOTAL_RETRIES = 3;
    private final Call<T> call;
    private int retryCount = 0;


    public CallbackWithRetry(Call<T> call) {
        this.call = call;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (!response.isSuccessful() && retryCount++ < TOTAL_RETRIES) {
            Log.v(TAG, "Retrying... (" + retryCount + " out of " + TOTAL_RETRIES + ")");
            retry();
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (t.getLocalizedMessage() != null)
            Log.e(TAG, t.getLocalizedMessage());
        if (retryCount++ < TOTAL_RETRIES) {
            Log.v(TAG, "Retrying... (" + retryCount + " out of " + TOTAL_RETRIES + ")");
            retry();
        }
    }

    private void retry() {
        call.clone().enqueue(this);
    }
}