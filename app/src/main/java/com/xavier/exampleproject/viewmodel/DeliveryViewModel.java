package com.xavier.exampleproject.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.xavier.exampleproject.model.Delivery;
import com.xavier.exampleproject.model.response.DeliveryResponse;
import com.xavier.exampleproject.network.DeliveryRepository;

import java.util.List;

public class DeliveryViewModel extends AndroidViewModel {
    private static final String TAG = "DeliveryViewModel";

    private DeliveryRepository deliveryRepository;
    private LiveData<DeliveryResponse> deliveryResponseLiveData;

    public DeliveryViewModel(@NonNull Application application) {
        super(application);
        deliveryRepository = DeliveryRepository.getInstance(application);
    }

    public void getFromAPI(Context context, int offset, int limit) {
        deliveryResponseLiveData = deliveryRepository.getDeliveryList(context, offset, limit);
    }

    public LiveData<List<Delivery>> getDeliveryListLiveData() {
        return deliveryRepository.getDeliveryListLiveData();
    }

    public LiveData<Delivery> getDeliveryLiveData(int deliveryId){
        return deliveryRepository.getDeliveryLiveData(deliveryId);
    }

    public void insertInDB(Delivery delivery) {
        deliveryRepository.insertInDB(delivery);
    }

    public void updateInDB(Delivery delivery) {
        deliveryRepository.updateInDB(delivery);
    }

    public LiveData<DeliveryResponse> getDeliveryRepository() {
        return deliveryResponseLiveData;
    }
}
