package com.xavier.exampleproject.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.xavier.exampleproject.model.Delivery;

import java.util.List;

@Dao
public interface DeliveryDao {
    @Query("SELECT * FROM delivery ORDER BY id")
    LiveData<List<Delivery>> getAllDeliveryData();

    @Query("SELECT * FROM delivery WHERE id = (:id)")
    LiveData<Delivery> getDelivery(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Delivery delivery);

    @Update
    void update(Delivery delivery);

    @Query("DELETE FROM delivery")
    void deleteAllDelivery();
}