package com.xavier.exampleproject.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.xavier.exampleproject.model.Delivery;
import com.xavier.exampleproject.model.Location;

@Database(entities = {Delivery.class, Location.class}, version = 1)
public abstract class AppDB extends RoomDatabase {
    public abstract DeliveryDao deliveryDao();

    private static AppDB instance;

    public static AppDB getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, AppDB.class, "deliveryDB")
                    .build();
        }
        return instance;
    }

    public static void destroyInstance() {
        instance = null;
    }
}