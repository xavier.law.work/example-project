package com.xavier.exampleproject.ui.page;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;
import com.xavier.exampleproject.R;
import com.xavier.exampleproject.adapter.DeliveryAdapter;
import com.xavier.exampleproject.db.AppDB;
import com.xavier.exampleproject.model.Delivery;
import com.xavier.exampleproject.network.DeliveryRepository;
import com.xavier.exampleproject.utils.EndlessRecyclerViewScrollListener;
import com.xavier.exampleproject.viewmodel.DeliveryViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @BindView(R.id.root)
    LinearLayout root;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    @BindView(R.id.rv_delivery_list)
    RecyclerView rv_delivery_list;

    DeliveryAdapter deliveryAdapter;
    DeliveryViewModel deliveryViewModel;

    List<Delivery> deliveryList = new ArrayList<>();

    int limit = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //toolbar setting
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        toolbar.setTitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));

        //new viewModel
        deliveryViewModel = new DeliveryViewModel(getApplication());

        //get list from api
        getDeliveryList(0, limit);

        //view observe liveData(viewModel) observe api
        deliveryViewModel.getDeliveryRepository().observe(this, apiDeliveries -> {
            if (apiDeliveries != null) {
                for (Delivery apiDelivery : apiDeliveries) {
                    deliveryViewModel.getDeliveryLiveData(apiDelivery.getId()).observe(MainActivity.this, delivery -> {
                        if (delivery != null) {
                            //if object exist, update
                            deliveryViewModel.updateInDB(apiDelivery);
                        } else {
                            //if object not exist, insert
                            deliveryViewModel.insertInDB(apiDelivery);
                        }
                    });
                }
                deliveryList.addAll(apiDeliveries);
            } else {
                //show error if no network
                Snackbar.make(root, R.string.network_error, Snackbar.LENGTH_LONG).show();
                //get data from db if no network
                deliveryViewModel.getDeliveryListLiveData().observe(this, deliveries -> {
                    deliveryList.addAll(deliveries);
                });
            }

            if (swipe_refresh_layout.isRefreshing())
                swipe_refresh_layout.setRefreshing(false);

            setupRecyclerView();
        });

        //swipe down to refresh list
        swipe_refresh_layout.setOnRefreshListener(() -> {
            deliveryList.clear();
            getDeliveryList(0, limit);
        });
    }


    @Override
    protected void onDestroy() {
        //clear resources
        AppDB.destroyInstance();
        DeliveryRepository.destroyInstance();

        super.onDestroy();
    }

    public void getDeliveryList(int offset, int limit) {
        deliveryViewModel.getFromAPI(getApplicationContext(), offset, limit);
    }

    public void setupRecyclerView() {
        if (deliveryAdapter == null) {
            //setup adapter
            deliveryAdapter = new DeliveryAdapter(MainActivity.this, deliveryList);
            deliveryAdapter.setOnItemClickListener((delivery, position) -> {
                Intent i = new Intent();
                i.setClass(MainActivity.this, DetailActivity.class);
                i.putExtra("deliveryId", delivery.getId());
                startActivity(i);
            });
            LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
            rv_delivery_list.setLayoutManager(layoutManager);
            rv_delivery_list.setAdapter(deliveryAdapter);

            //load more when scroll to bottom
            rv_delivery_list.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    Log.d(TAG, "onLoadMore");
                    getDeliveryList(page * limit, limit);
                }
            });
        } else {
            //notify adapter
            rv_delivery_list.post(() -> {
                //prevent error that recyclerview is computing layout
                deliveryAdapter.notifyDataSetChanged();
            });
        }
    }
}
