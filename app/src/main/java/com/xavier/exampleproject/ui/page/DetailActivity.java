package com.xavier.exampleproject.ui.page;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.xavier.exampleproject.R;
import com.xavier.exampleproject.viewmodel.DeliveryViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = "DetailActivity";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.iv_delivery_image)
    ImageView iv_delivery_image;

    @BindView(R.id.tv_delivery_description)
    TextView tv_delivery_description;

    DeliveryViewModel deliveryViewModel;

    int deliveryId;

    GoogleMap googleMap;
    double latitude;
    double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        //google map setting
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map_fragment);
        if (mapFragment != null)
            mapFragment.getMapAsync(this);

        //toolbar setting
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        toolbar.setTitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));
        toolbar.setNavigationOnClickListener(view -> finish());

        //get deliveryId from intent
        deliveryId = getIntent().getIntExtra("deliveryId", 0);
        Log.d(TAG, "deliveryId " + deliveryId);

        //new viewModel
        deliveryViewModel = new DeliveryViewModel(getApplication());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady");
        this.googleMap = googleMap;

        deliveryViewModel.getDeliveryLiveData(deliveryId).observe(DetailActivity.this, delivery -> {
            latitude = delivery.getLocation().getLat();
            longitude = delivery.getLocation().getLng();

            LatLng marker = new LatLng(latitude, longitude);

            //add a marker in the map, and move the camera and zoom to the marker.
            DetailActivity.this.googleMap.addMarker(new MarkerOptions().position(marker).title("Marker"));
            DetailActivity.this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker, 17.0f));

            //load image
            Picasso.get()
                    .load(delivery.getImageUrl())
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .centerCrop()
                    .fit()
                    .into(iv_delivery_image);

            //set description
            tv_delivery_description.setText(delivery.getDescription()
                    .concat(" at ").concat(delivery.getLocation().getAddress()));
        });
    }
}
