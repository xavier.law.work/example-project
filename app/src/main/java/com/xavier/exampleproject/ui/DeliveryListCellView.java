package com.xavier.exampleproject.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import com.squareup.picasso.Picasso;
import com.xavier.exampleproject.R;
import com.xavier.exampleproject.model.Delivery;
import com.xavier.exampleproject.model.Location;
import com.xavier.exampleproject.viewmodel.DeliveryViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeliveryListCellView extends RelativeLayout {
    private static final String TAG = "DeliveryListCellView";

    Context context;

    @BindView(R.id.iv_delivery_image)
    ImageView iv_delivery_image;

    @BindView(R.id.tv_delivery_description)
    TextView tv_delivery_description;

    public DeliveryListCellView(Context context) {
        super(context);
        init(context);
    }

    public DeliveryListCellView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DeliveryListCellView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public DeliveryListCellView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes, Context context1) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    protected void init(Context context) {
        this.context = context;
        inflate(context, R.layout.view_delivery_list_cell, this);
        ButterKnife.bind(this);
    }

    public void bindModel(Delivery delivery) {
        Log.d(TAG, "bindModel");
        //prevent null data
        if (delivery != null) {
            Picasso.get()
                    .load(delivery.getImageUrl())
                    .error(R.mipmap.ic_launcher)
                    .placeholder(R.mipmap.ic_launcher)
                    .fit()
                    .centerCrop()
                    .into(iv_delivery_image);

            tv_delivery_description.setText(delivery.getDescription()
                    .concat(" at ").concat(delivery.getLocation().getAddress()));
        }
    }
}
