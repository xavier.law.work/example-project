package com.xavier.exampleproject.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xavier.exampleproject.R;
import com.xavier.exampleproject.model.Delivery;
import com.xavier.exampleproject.model.Location;
import com.xavier.exampleproject.ui.DeliveryListCellView;
import com.xavier.exampleproject.ui.page.DetailActivity;

import java.util.List;

public class DeliveryAdapter extends RecyclerView.Adapter<DeliveryAdapter.DeliveryViewHolder> {
    private static final String TAG = "DeliveryAdapter";

    private Context context;
    private List<Delivery> deliveryList;

    public DeliveryAdapter(Context context, List<Delivery> deliveryList) {
        this.context = context;
        this.deliveryList = deliveryList;
    }

    @NonNull
    @Override
    public DeliveryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        DeliveryListCellView view = new DeliveryListCellView(parent.getContext());
        DeliveryViewHolder holder = new DeliveryViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DeliveryViewHolder holder, int position) {
        holder.itemView.setPadding(
                0,
                position > 0 ? (int) context.getResources().getDimension(R.dimen.default_padding) : 0,
                0,
                0);
        holder.itemView.bindModel(deliveryList.get(position));
        holder.itemView.setOnClickListener(view -> {
            if (onItemClickListener != null)
                onItemClickListener.onItemClick(deliveryList.get(position), position);
        });
    }

    @Override
    public int getItemCount() {
        return deliveryList.size();
    }

    public class DeliveryViewHolder extends RecyclerView.ViewHolder {
        DeliveryListCellView itemView;

        public DeliveryViewHolder(@NonNull DeliveryListCellView itemView) {
            super(itemView);
            this.itemView = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private OnItemClickListener onItemClickListener;

    //define interface
    public interface OnItemClickListener {
        void onItemClick(Delivery delivery, int position);
    }
}
