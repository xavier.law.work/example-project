package com.xavier.exampleproject.model;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "delivery")
public class Delivery {
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;

    @ColumnInfo
    @SerializedName("description")
    @Expose
    private String description;

    @ColumnInfo
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;

    @Embedded
    @SerializedName("location")
    @Expose
    private Location location;

    public void setId(int id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Location getLocation() {
        return location;
    }


    @Override
    public String toString() {
        return "Delivery{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", location=" + location +
                '}';
    }
}
