package com.xavier.exampleproject.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "location")
public class Location {
    @PrimaryKey(autoGenerate = true)
    private int locationId;

    @ColumnInfo
    @SerializedName("lat")
    @Expose
    private double lat;

    @ColumnInfo
    @SerializedName("lng")
    @Expose
    private double lng;

    @ColumnInfo
    @SerializedName("address")
    @Expose
    private String address;

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Location{" +
                "locationId=" + locationId +
                ", lat=" + lat +
                ", lng=" + lng +
                ", address='" + address + '\'' +
                '}';
    }
}
