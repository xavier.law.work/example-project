# Example Project

This is a Example Project

## How to set google api key

1. go to the link [google-console](https://console.developers.google.com) and login to your account.
2. Go to `Credentials`
3. Copy the google api key
4. Go to the project->res->values->google_maps_api.xml
5. Replace the key in the TODO
```     
<string name="google_maps_key" templateMergeStrategy="preserve" translatable="false">
//TODO: replace your api key
</string>
```
6. The API key should start with 
```
AIzaxxxxxxxxxxxxxxxxxxxxxxxxxxx
```
